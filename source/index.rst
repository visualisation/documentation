.. Vis documentation master file, created by
   sphinx-quickstart on Tue Sep  8 11:52:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for visualization
===============================

.. toctree::
   :maxdepth: 4
   :caption: Contents:
   :glob:

   Paraview/index


.. only:: html

          Indices and tables
          ==================

          * :ref:`genindex`
          * :ref:`search`
