.. _texture-map-to-plane:

Add an image as texture (plane) in the background
================================================================================



.. note::
   This document is part of a series teaching how to :ref:`create-image-sea-surface-speeds`.

   See also :ref:`texture-map-to-sphere`

   You can find beautiful images on the `NASA Blue Marble Next Generation page <https://visibleearth.nasa.gov/collection/1484/blue-marble>`_ For this example, you can use `a low-res version of the February image <https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74268/world.topo.200402.3x5400x2700.jpg>`_.


As our ocean data set has *holes* on the land points, we have to first create a plane *behind* it, before we can attach an image of Earth.

.. image:: 000-ocean-with-holes.png

As a first step, we figure out the size of *Earth* in paraview. For this we need the ``Information View`` (View-> Information). There we look for the section ``Bounds`` and note the extreme values. The x-range is from roughly -300 to 300, the y-range is from roughly -131 to 150. The lower bound of the y-range is a bit weird, because Antarctica is cut off, because the oceans don't reach 90 deg S. Now we know our plane needs to be -300 to 300 by -150 to 150, and just below z=0.

.. image:: 00a-find-data-size.png

We can get a plane from the ``Sources`` menu, e.g. from ``Geometric Shapes``.

.. image:: 00b-get-plane.png

Here we adjust the x coordinates to +/- 300 and the y coordinates to +/- 150, keeping the signs as they were. As z-coordinate we use -0.1 to put this plane just below the ocean.

.. image:: 00c-new-coordinates.png

After clicking ``Apply``, we should have all land areas filled with white.

Now we can add the texture itself. Chose ``Filters->Search...`` and type ``texture``. You will be presented with the three different texture filters. Chose ``Texture Map to Plane`` and hit ``Enter``

.. image:: 01-find-filter.png

Now you first have to click apply before you can set the texture (for whatever reason)

.. image:: 02-apply.png

Now use the text box and search for texture. Click the ``Texture`` dropdown menu, and chose load.


.. image:: 03-find-texture-field.png

Pick the small blue marble from the directory with the sample data (or use any one of NASA's Blue Marble images, e.g.  `the low-res version of the February image <https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74268/world.topo.200402.3x5400x2700.jpg>`_. (Thanks, NASA for that great service!).


.. image:: 04-texture-file.png

You should have Earth peeking out of the holes in the ocean.


.. image:: 05-texture-on-earth.png


Next, you can :ref:`camera-parallel-projection` to reduce projection artifacts.
