.. _texture-map-to-sphere:

Add an image as texture on a sphere
========================================

Create a sphere and follow the steps for mapping a texture to a plane in :ref:`texture-map-to-plane`, replacing *plane* by *sphere*. For CDI reader data (e.g. ICON), the radius should be 200.

Some adjustments are necessary. If you use a custom-created sphere, increase the resolution in teta and phi in the properties of the sphere (not the texture mapping).

.. image:: ../../Sources/Sphere/sphere-resolution.png

To avoid a seam set the theta-range to 359.999

.. image:: ./theta-range.png

To match the projection to that of icon data set all scalings to -.999

.. image:: ./scale-texture.png
