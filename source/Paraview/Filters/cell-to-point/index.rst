.. _cell-or-point:

Check if you have cell or point data
==================================================


.. note::
   This document is part of a series teaching how to :ref:`create-palmod-combined`.

Some filters are picky with regard to their input data type and require cell data or point data.

Use the dropdown for choosing the object color to check which variables of which types are available.
In this example topo exists as cell and as point data (because we just converted it). The topo with a dot in front of it is point data. That with a box is cell data.

.. image:: cell-or-point.png


If neccessary you can



.. _convert-cell-to-point:

Convert cell data to point data
================================


To convert cell data to point data, do ``Filters->Search`` and enter ``cell da``, use ``Enter`` to cofirm.

.. image::
   cell2point.png

In the settings of the calculator activate passing of cell data.
If you have many arrays in your input data, you might only want to process the data of relevance for this use.

.. image::
	    pass-cell.png


(Continue with :ref:`extrude-land-surface`)
