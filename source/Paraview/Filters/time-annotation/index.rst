.. _time-annotation:

Add a time annotation filter
========================================
The filter we are looking for is NetCDF Time annotation. It is not available on all systems (e.g. the default paraview).
