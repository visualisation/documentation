.. _extrude-land-surface:

Extrude a land surface based on topography
============================================================


.. note::
   This document is part of a series teaching how to :ref:`create-palmod-combined`.


If you don't already have it (files in the example are already prepared), you will need a (high-resolution) topography, e.g. from ``cdo -topo``.
Remap your data to the resolution of the topography. Something along the lines of::

  cdo -f nc -topo topo.nc # will create a topo on a 0.5 deg lat/lon grid.
  cdo -merge -remapnn,topo.nc MYFILE topo.nc MYFILE_TOPO.nc

.. note::
   The calculator needs point data for processing. Often the topo data ends up being cell data.  :ref:`cell-or-point` and, if necessary,  :ref:`convert-cell-to-point` before applying the calculator.

Make sure the ``Attribute Type`` Setting in your calculator is on ``Point Data``, select ``Coordinate Results``, give your resulting coordinates a reasonable name, and use the equation (assuming your topography variable is called topg and in m, this yields a vertical exaggeration of 100) ::

  (1+topg*100/6731000)*(coordsX*iHat+coordsY*jHat+coordsZ*kHat)

Where the first paren scales the topography to a strong extrusion around a unit sphere, and the second part is the vector from the origin to the current grid point's position. Your topography variable should appear in the ``Scalars`` dropdown menu. If not, go back to :ref:`cell-or-point`.




.. image::
   calculator-settings.png


.. note::
   For plane projections (Mercator, Mollweide / ...) use something along the lines of::

     (1+topo/20000) * coordsZ*kHat + (coordsX*iHat+coordsY*jHat)


(return to :ref:`vegetation-data`)
