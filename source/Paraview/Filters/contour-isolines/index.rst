.. _contour-isolines:

Use the contour filter to display isolines
===========================================

Add a contour filter

.. image::
   add-contour.png

Chose the variable, you want to contour by, use the red x to get rid of the default contour level, use the *scale* icon (below + and -) to create a number series, (see below), finally select the levels ``100`` and the ``0`` and use the ``+`` to add ``-50`` and ``50`` levels below them.

.. image::
   contour-levels.png



.. image::
   number-series.png

Adjust the line width to ``5`` by searching for ``line`` in the search box in the top and then adjusting it.

.. image::
   line-width.png

Select ``BrBG`` as colormap, invert it, and set the value range to ``-100`` to ``100``.
