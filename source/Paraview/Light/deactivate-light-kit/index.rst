.. _deactivate-light-kit:

Deactivate the light kit
========================


.. note::
   This document is part of a series teaching how to :ref:`create-image-sea-surface-speeds`




By default, Paraview creates a couple of light sources in positions that are fixed relative to the viewer. This light sources allow for shadows and depth perception. However this comes at the cost of brilliance of colors.

.. image:: 01-get-light-inspector.png

Use the checkbox in the top left corner to deactivate the light kit.

.. image:: 02-light-inspector.png


If you followed the whole series of  :ref:`create-image-sea-surface-speeds`, your result should look like this:

.. image:: 03-ocean-currents-no-light-kit.png

Next you can :ref:`texture-map-to-plane`.
