.. _save-colormap-preset:

Save the colormap as a preset
========================================

.. note::
   This document is based on a series teaching how to :ref:`display-transparent-clouds`


Go to the colormap editor, chose the ``Save to preset`` option next to the preview.

.. image:: save-colormap.png


You can give the colormap a name, and chose to also save the opacity information.

.. image:: name-dialogue.png

If this name cannot be found in the following pop-up dialogue, search for ``Preset``  in the top menu and rename...


.. image:: change-name.png

This is the end of the tutorial :ref:`display-transparent-clouds`. Feel free to  :ref:`save-state`, and :ref:`export-animation`.
