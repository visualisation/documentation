.. _invert-colormap:

Invert the Colormap
========================================

.. note::
   This document is part of a series teaching how to :ref:`create-palmod-combined`

Go to the Colormap editor (``View -> Color Map Editor Window``), and choose the Black and White icon right of the display.

.. image:: invert.png


(Return to :ref:`vegetation-data`)

