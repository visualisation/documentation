.. _rescale-colormap-to-custom:

Rescale the colormap to a custom range
========================================

.. note::
   This document is part of a series teaching how to :ref:`create-image-sea-surface-speeds`

Use this button to rescale your color map to a custom range or chose a custom color for monochromatic displays

.. image:: 09-rescale-to-custom.png

Chose 0 to 1 to get this image:

.. image:: 10-rescaled-to-custom.png


you can now :ref:`deactivate-light-kit` to make the colors stand out even better.
