.. _colormap-transparent-clouds:

Adjust the color map for a transparent display of clouds
============================================================

.. note::
   This document is based on a series teaching how to :ref:`display-transparent-clouds`


Here, we display clouds as white overlay with the opacity adjusted by the value of the field.

First :ref:`rescale-colormap-to-custom` to 0-1. We have alpbedo (alpha) values between 0 and 1.

Now go to the color map editor

Delete the center point in the bar with the color gradient (saves the effort of setting it to white).

Activate manual editing of the transfer function (the gear at the bottom of the colormap display, here shown for a different colormap).

``Enable opacity mapping for surfaces`` in the checkboxes below the bar.


Set the R/G/B values to ``1`` in the ``Color transfer function values`` section, and reduce the maximum opacity to ``0.8`` in the ``Opacity transfer function``. Play with the opacity value to find your favorite compromise between visibility of clouds and background.

Things should now be changed from

.. image:: fields-to-change.png

to

.. image:: fields-changed.png

Your result should look like

.. image:: transparent-clouds-ocean-speeds.png


You can deactivate the display of the color legend with the top left color legend icon.

.. image:: deactivate-color-legend.png
           

Don't forget to :ref:`save-colormap-preset`.
