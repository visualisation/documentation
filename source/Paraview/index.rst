.. Vis documentation master file, created by
   sphinx-quickstart on Tue Sep  8 11:52:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for visualization with Paraview
===================================================

Start by having a look at :ref:`main-screen` or take a guided tour and :ref:`create-image-sea-surface-speeds`

Or chose from the full menue:

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   *
   Examples/index
   */index
