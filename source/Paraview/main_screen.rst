.. _main-screen:

The Paraview main screen
===============================

Here's the paraview main screen

.. image:: main_screen.png


The windows for all sub-modules can be anchored in the main screen on either side or at the top/bottom. Here, we have the pipeline above a block featuring properties, and information on the left. The right hand side is shared between the Color Map Properties and the Light Inspector.
