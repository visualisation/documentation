.. _activate_the_cdi_reader_plugin:

Activate the CDI reader plugin
--------------------------------


.. note::
   This document is part of a series teaching how to :ref:`create-image-sea-surface-speeds`

For loading ICON data, we need to activate the CDI reader plugin (on Mistral it's active by default)

Chose Tools-> Manage Plugins


.. image:: 00a-load-plugin.png

Select the CDIReader plugin, open the detailed view, activate ``Auto Load``, and Chose ``Load selected`` at the bottom. The ``Status`` should now be ``Loaded``, and ``Load Selected`` should be greyed out.


.. image:: 00b-activate-cdi-plugin.png


With the cdi reader loaded, you can  :ref:`load-2d-icon`
