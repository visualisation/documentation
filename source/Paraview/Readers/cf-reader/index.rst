.. _netcdf-cf-reader:

The NetCDF CF reader
========================================

.. note::
   This document is part of a series teaching how to :ref:`create-palmod-combined`

You can use the NetCDF CF reader to load data on regular (and distorted) grids.

Issues:

* The reader cannot handle negative time information (will give an error along the lines of "start + count exceeds dimension bounds").
* The reader cannot display distorted grids in the plane (e.g. most ocean models)
* The reader cannot handle a 2D field that has a depth axis (use ``cdo -setzaxis,surface INFILE OUTFILE`` or ``ncwa -a NAME_OF_DEPTH_DIMENSION INFILE OUTFILE`` to prepare files; affects a lot of MPI-OM output).
* Ocean models tend to give positive values for depth. That requires a couple switches in the reader, see :ref:`load-nemo`.
* For MPI-OM velocities, first use ``cdo -mrotuvb`` to get data to North/South East/West orientation, and interpolate u and v onto the cell centers.
* Nemo grids cause weird hick-ups.
* One file with many time steps tends to be a lot faster than many files with one timestep each.

.. _open-with-cf-reader:

Open a file with the standard NetCDF Reader
============================================

Use ``File->Open`` or the top-left icon (see :ref:`main-screen`), chose the desired file and select ``NetCDF Reader`` when prompted.

.. image:: chose-reader.png


For a spherical projection use ``Spherical Coordinates`` and (for most cases) use ``Replace Fill Value With Nan``

.. image:: spherical-nans.png


(Return to :ref:`vegetation-data`)
