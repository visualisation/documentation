.. _load-nemo:

Load NEMO 3D data with the netCDF CF reader
==================================================

In Paraview 5.7, you can kind of load nemo data using the netCDF CF reader.

Settings that get you some idea of the output are

``Spherical Coordinates``

``Vertical Scale = -0.1``

``Vertical Bias = 1000``

``Replace Fill Value with NaN``

.. image:: ./nemo-with-netCDF-CF-reader.png
