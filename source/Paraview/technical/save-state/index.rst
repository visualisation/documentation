.. _save-state:

Save a state file
============================================

There are two possible formats for saving the state (recipe) of your paraview session: ``.pvsm``, the paraview-native XML format, and ``.py`` -- python executables that can be executed using ``pvbatch``. The python export is not as stable as ``.pvsm``, so it might be a good idea to **first save your state to ``.pvsm``** and save an additional ``.py`` copy after this.


.. image:: file-save-state.png


**It has proved very useful to have older states at hand.** Paraview has the *feature* of grouping files that end on a number, so _a _b _c naming has proved useful. Using a version control system is even better. :)
