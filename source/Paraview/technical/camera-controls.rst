.. _camera-controls:


Adjust / check the camera controls
==================================================

In the Settings you can adjust or just check the mouse bindings for controlling the camera.


.. image:: preferences/controls.png
