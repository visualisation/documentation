.. _open-settings:

Open the Paraview settings
================================================

.. image:: get-preferences.png
   :alt: Paraview->Preferences or cmd-, on mac

.. image:: get-preferences-linux.png
   :alt: Edit->Settings
