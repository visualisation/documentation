.. _make-paraview-save-on-quit-or-crash:

Make Paraview save a state on quitting or crashing
================================================================================

:ref:`open-settings` and use the text box in the ``General`` tab to seach for ``save``, activate both options, and ``Apply``.


.. image:: activate-options.png
