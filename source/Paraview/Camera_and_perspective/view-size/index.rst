.. _set-view-size:


Set the view size
====================

.. note::
   This document is part of a series teaching how to :ref:`create-image-sea-surface-speeds`.


When preparing to export images for a video or similar, forcing paraview to display the image with *correct* proportions makes things a lot easier.

Go to ``View->Preview`` and chose the aspect ratio and size of the final product.

.. image:: view-size-fhd.png

Your screen display will be adjusted accordingly

.. image:: fhd-preview.png

Next you could :ref:`export-animation`.
