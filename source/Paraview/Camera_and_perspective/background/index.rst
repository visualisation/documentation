.. _set-background:

Set the background (color/gradient)
==================================================

.. note::
   This document is part of a series teaching how to :ref:`create-image-sea-surface-speeds`.


.. _set-current-background:

Set the background of the current plot
--------------------------------------------------

To set the background of your current project go to properties of any object, type ``backg`` into the text field, and chose a background of your liking. Chosing pink might help looking for holes in your data - or making your plot a bit *special*.

.. image:: set-background.png


.. image:: pink-background.png

Next you could :ref:`prettify-colorbar`


.. _set-default-background:

Set the default background
----------------------------------------


To set the default background :ref:`open-settings` and adjust it in the ``Color Palette`` tab

.. image:: set-default-background.png
