.. _camera-parallel-projection:

Switch to camera parallel projection
========================================

.. note::
   This document is part of a series teaching how to :ref:`create-image-sea-surface-speeds`.

If you are using paraview for 2D mapping, switching to camera parallel projection helps removing projection artifacts. This will remove any depth effects in the perspective, so objects that are further away are displayed at the same size as nearby objects.

Go to the properties panel of any object, type :code:`paral` into the search box, and activate :code:`Camera Parallel Projection`.


.. image:: activate_camera_parallel_projection.png

Next you could :ref:`set-background` (and its default).
