.. _create-image-sea-surface-speeds:

Create an image of sea surface speeds
===========================================

These steps will get you from


.. image:: 00-empty-main.png

via

.. image:: 10-ocean-speeds.png

to

.. image:: 2d-ocean-with-earth-and-adjustments.png

.. note::
   You can find a video recording covering this material at https://youtu.be/TCmnBme0OWA?t=2144, and the material at https://github.com/ESiWACE/hpda-vis-training (run the make file of the Training2021/Session2 folder).
   You can download all files by::

     git clone git@github.com:ESiWACE/hpda-vis-training.git
     cd hpda-vis-training/Training2021/Session2/
     make

	   
.. note::
   Whatever you do with paraview, keep saving state files (``File -> Save State``) (see :ref:`save-state`) at regular intervals. You can additionally :ref:`make-paraview-save-on-quit-or-crash`.

You might need to :ref:`activate_the_cdi_reader_plugin`

With the cdi reader loaded, you can  :ref:`load-2d-icon`

You can now :ref:`calculator-uv-speed`

With the data loaded, you can :ref:`chose-BuGn`

For tuning your resulting image, you can

* :ref:`rescale-colormap-to-custom`
* :ref:`deactivate-light-kit`
* :ref:`texture-map-to-plane` to fill the holes in the ocean.
* :ref:`camera-parallel-projection` to reduce projection artifacts
* :ref:`set-background` (and its default).
* :ref:`prettify-colorbar`

Finally you can de-activate the display of the coordinate system in the bottom right by clicking the icon with the eye and the coordinate axis in the right of the menu (see :ref:`main-screen`).

To prepare for export (as/for a video) you should :ref:`set-view-size`  before you :ref:`export-animation`.

Here, you can :download:`Download a state file with all features <2d-ocean-with-earth-and-adjustments.pvsm>`. You will need to  :ref:`activate_the_cdi_reader_plugin` before loading it and chose the directory with your downloaded input files on loading the state file to make it work.

You can continue with :ref:`display-transparent-clouds`.
