.. _paraview-palmod-pre-processing:

Pre-Processing for a multi-variable image / animation
============================================================

* For general advice on the NetCDF CF reader see :ref:`netcdf-cf-reader`
* All files need the same time axis. Different models will usually use different specifications, sending you straight to hell. One solution that works: resample all files to the same number of time steps, then use::

    ncks -Av time MASTER_FILE FILE1
    ncks -Av time MASTER_FILE FILE2
    ...

  to get all on the same time axis. If that causes trouble (e.g. because of float/double conversions), look into ``cdo -settaxis``.

  .. warning::
     The time axis must not contain negative values (don't ask how long it took to figure this out)


* For extruding with topography it's best to have the topography in the same file as the data of interest. See :ref:`extrude-land-surface` for hints.

* MPI-OM 2d data needs::

    cdo -setzaxis,surface IN OUT

  to get rid of the layer-dimension that does not have any content.
