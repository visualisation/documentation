.. _load-icebergs:

Load the iceberg data
========================================

* Load the iceberg data from `surf_fw_out_pv_777.nc <https://swiftbrowser.dkrz.de/public/dkrz_8656c91ce0734327b6dc867fc5b6b068/ESiWACE2-Public/Paraview%20Sample%20Data/PalMod_example/>`_
* add a calculator that scales and outputs to ``surf_fw_out``::

    surf_fw_out*86400*360

* use a logarithmic color scale with the color map ``Blues``, invert the color map, and set min and max to 0.01 and 1. This should get you to

.. image::
   icebergs-added.png


Time to :ref:`load-ice-sheets`
