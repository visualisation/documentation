.. _load-rivers:

Load the rivers
===============

.. note::
   This document is part of a series teaching how to :ref:`create-palmod-combined`


* Load the river runoff data from `friv_777.nc <https://swiftbrowser.dkrz.de/public/dkrz_8656c91ce0734327b6dc867fc5b6b068/ESiWACE2-Public/Paraview%20Sample%20Data/PalMod_example/>`_

* :ref:`convert-cell-to-point` and :ref:`extrude-land-surface` with the slightly modified equation::

    (6371500+.5*(topg+abs(topg))*100)/6371000*(coordsX*iHat+coordsY*jHat+coordsZ*kHat)

putting rivers in ocean areas above sea level, and the rivers clearly above ground.

* Create a monochromatic blue color map with transparency (see :ref:`colormap-transparent-clouds`)


(return to :ref:`create-palmod-combined`)
