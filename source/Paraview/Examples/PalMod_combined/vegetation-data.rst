.. _vegetation-data:

Load the vegetation data
========================================

.. note::
   This document is part of a series teaching how to :ref:`create-palmod-combined`.


* Open `veg_ratio_max_nn_777.nc <https://swiftbrowser.dkrz.de/public/dkrz_8656c91ce0734327b6dc867fc5b6b068/ESiWACE2-Public/Paraview%20Sample%20Data/PalMod_example/>`_ ( see :ref:`open-with-cf-reader` for instructions).
* Use the variable ``veg_ratio_max``
* Load the colormap ``Linear Green (GR4l)`` (see :ref:`chose-BuGn`).
* :ref:`invert-colormap`

* For the exact colormap of this example:

  *  Remove all points above 0.65 from the colormap (see :ref:`colormap-transparent-clouds` for hints on manipulating a color map).
  *  Save the colormap, open it in the text editor, :ref:`hack-colormap` to reset the maximum to .67, save it, and import it again.


Your data should look roughly like this:

.. image:: vegetation-loaded.png

* :ref:`convert-cell-to-point` and :ref:`extrude-land-surface`.

your file should look like this:

.. image:: vegetation-topo.png

(return to :ref:`create-palmod-combined`)
