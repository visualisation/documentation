.. _create-palmod-combined:

Create a multi-variable image / animation from a coupled ice sheet - climate simulation
=======================================================================================================

This example will show how to create an animation based on ocean, vegetation, solid earth, river runoff, and ice sheet data. This requires substantial pre-processing, that will only be outlined here. All preprocessed files can be downloaded from the `DKRZ SwiftBrowser <https://swiftbrowser.dkrz.de/public/dkrz_8656c91ce0734327b6dc867fc5b6b068/ESiWACE2-Public/Paraview%20Sample%20Data/PalMod_example/>`_

.. note::

   You can download all files by::

     git clone git@github.com:ESiWACE/hpda-vis-training.git
     cd hpda-vis-training/Training2021/Session2/
     make


.. image:: combined-image.png


In this example we assume a very basic level of familiarity with Paraview. If you feel lost in between, consider looking at :ref:`create-image-sea-surface-speeds`. This multi-variable visualization requires a substantial amount of :ref:`Pre-processing<paraview-palmod-pre-processing>`. We will skip these steps for now, as they are different for any two data sets.

.. toctree::
   :maxdepth: 2
   :caption: Steps:
   :glob:

   vegetation-data
   load-icebergs
   load-rivers
   load-ice-sheets

* Repeat the same exercise including :ref:`convert-cell-to-point` for ``vilma_ss_777.nc`` (variable ``topg`` for the topography and ``delta_topg`` for the isolines - copy and paste as appropriate). That should get you isolines on land.
* Also apply the contour to the pure Cell Data to Point Data filter, to get isolines on the ocean.

:download:`Download a complete pvsm state file here.<ice_sheet_complete.pvsm>`
