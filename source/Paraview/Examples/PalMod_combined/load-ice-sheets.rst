.. _load-ice-sheets:

Load the ice sheets
==============================

.. note::
   This document is part of a series teaching how to :ref:`create-palmod-combined`


* Load the ice sheet data from `pism_NH_777.nc <https://swiftbrowser.dkrz.de/public/dkrz_8656c91ce0734327b6dc867fc5b6b068/ESiWACE2-Public/Paraview%20Sample%20Data/PalMod_example/>`_

This file already is in point data (there are no bounds associated with the coordinates), so we don't need a CellData2PointData filter.

* Attach a calculator 
* select the calculator of the vegetation in the pipeline view, copy it, and paste it to the newly created calculator (i.e. copy the settings to the calculator of the ice sheet). 
* Change ``topg`` to ``ice_surf`` in the equation of the calculator.

.. image:: copy-calculator.png

* Color by ``velsurf_mag``, chose ``Use log scale when mapping data to colors``, load the colormap of your choice.

* :ref:`contour-isolines` to show the bedrock displacement, and re-activate the display of the ice sheet calculator in the pipeline browser.


Feel free to repeat the exercise for Antarctica. You might need to :ref:`convert-cell-to-point` before adding the calculator (for whatever reason). Now the isolines are added on the ice, but end at the coast.


(return to :ref:`create-palmod-combined`)
