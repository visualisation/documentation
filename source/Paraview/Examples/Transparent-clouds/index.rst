.. _display-transparent-clouds:

Display clouds as a semi-transparent overlay
==================================================


.. note::
   This document is based on a series teaching how to :ref:`create-image-sea-surface-speeds`. If you continue from this, or other work, don't forget to :ref:`save-state`. If not, you might want to :ref:`texture-map-to-plane` to get a nice background.

   Here, you can :download:`Download a state file with all features <../2D_ocean_example/2d-ocean-with-earth-and-adjustments.pvsm>` with the ocean currents. You will need to  :ref:`activate_the_cdi_reader_plugin` before loading it and chose the directory with your downloaded input files on loading the state file to make it work.


First, load ``clivi_cllvi_newtime.nc`` (see :ref:`load-2d-icon` for details).

Then add a calculator with the equation

.. code-block::

   (3/2 * (cllvi + clivi * 5) * 100)/ (3/2 * (cllvi + clivi * 5) * 100 + 7)

This is a rough conversion from vertically integrated cloud water and ice to optical density / albedo.

and call the result ``alpha`` (see :ref:`calculator-uv-speed`).

:ref:`colormap-transparent-clouds`

Disable the display of the color bar for the clouds (top left of :ref:`main-screen`).

Your result should look like

.. image:: transparent-clouds.png


Finally :ref:`save-state`, and :ref:`export-animation`.

:download:`Here is a state file with all features for reference<currents-and-clouds.pvsm>`.
