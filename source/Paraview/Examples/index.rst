.. _paraview-examples:

Paraview Examples
=================

Two introductory examples working with ICON data and building on each other

.. toctree::
   :maxdepth: 1
   :glob:
      
   2D_ocean_example/index
   Transparent-clouds/index
 
A more complex example working with MPI-ESM, PISM and VILMA:


.. toctree::
   :maxdepth: 1
   :glob:

   PalMod_combined/index


.. *
